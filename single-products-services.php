<?php get_header(); ?>

<?php if (have_posts()) : ?>

  <?php
  while (have_posts()) :
    the_post();
    $images = get_field('images');
    $short_description = get_field('short_description');
    $description = get_field('description');
    $brochure = get_field('brochure');
    $project_category = get_field('project_category');
  ?>

    <div class="flex flex-col z-20 lg:flex-row">
      <div class="w-full order-2 lg:w-3/5 lg:order-1">
        <?php
        if ($images) : ?>
          <?php if (count($images) > 1) { ?>
            <div id="product-swiper" class="swiper lg:rounded-r-xl">
              <div class="swiper-wrapper lg:rounded-r-xl">
                <?php foreach ($images as $image) : ?>
                  <div class="swiper-slide bg-slate-200 aspect-video">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="h-full w-full object-cover" />
                  </div>
                  <li>
                    <a href="<?php echo esc_url($image['url']); ?>">
                      <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </a>
                  </li>
                <?php endforeach; ?>
              </div>
              <div class="swiper-pagination"></div>
              <div class="swiper-buttons flex gap-x-2 absolute right-4 bottom-4 z-10">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
              </div>
            </div>
            <script>
              // Product Swiper
              const productSwiper = new Swiper('#product-swiper', {
                direction: 'horizontal',
                loop: false,
                autoplay: {
                  delay: 5000,
                  disableOnInteraction: false,
                },
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
                navigation: {
                  nextEl: '.swiper-button-next',
                  prevEl: '.swiper-button-prev',
                },
              });
            </script>
          <?php } else { ?>
            <div class="lg:rounded-r-xl overflow-hidden">
              <div class="bg-slate-200 aspect-video">
                <img src="<?php echo esc_url($images[0]['url']); ?>" alt="<?php echo esc_attr($images[0]['alt']); ?>" class="h-full w-full object-cover" />
              </div>
            </div>
          <?php } ?>
        <?php endif; ?>
      </div>
      <div class="w-full order-1 lg:w-2/5 lg:pl-10 lg:order-2 lg:pb-48">
        <div class="lg:max-w-md flex flex-col py-6 px-4 border-t border-stone-100 border-solid lg:border-t-0 lg:p-0 md:h-full">

          <div class="mb-6 lg:mb-auto">
            <?php
            $pagelist = get_pages('post_type=products-services&sort_column=menu_order&sort_order=asc');
            $pages = array();
            //print_r($pagelist);
            foreach ($pagelist as $page) {
              $pages[] += $page->ID;
            }

            $current = array_search(get_the_ID(), $pages);
            $prevID = $pages[$current - 1];
            $nextID = $pages[$current + 1];
            ?>

            <div class="navigation flex gap-x-4 lg:gap-x-8 lg:pt-4">
              <div class="w-1/2 text-left">
                <?php if (!empty($prevID)) { ?>
                  <a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>" class="flex gap-2 justify-start text-stone-700 hover:text-blue-500">
                    <div class="flex-none pt-0.5">
                      <?php echo interdesign_icon(array(
                        'icon'  => 'arrow-long',
                        'group'  => 'content',
                        'size'  => 20,
                        'class'  => 'fill-blue-500 rotate-180',
                      )); ?>
                    </div>
                    <div>
                      <?php echo get_the_title($prevID); ?>
                    </div>
                  </a>
                <?php } ?>
              </div>
              <div class="w-1/2 text-right">
                <?php if (!empty($nextID)) { ?>
                  <a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>" class="flex gap-2 justify-end text-stone-700 hover:text-blue-500">
                    <div>
                      <?php echo get_the_title($nextID); ?>
                    </div>
                    <div class="flex-none pt-0.5">
                      <?php echo interdesign_icon(array(
                        'icon'  => 'arrow-long',
                        'group'  => 'content',
                        'size'  => 20,
                        'class'  => 'fill-blue-500',
                      )); ?>
                    </div>
                  </a>
                <?php } ?>
              </div>
            </div><!-- .navigation -->

          </div>

          <div class="md:mt-auto">
            <header class="entry-header lg:mb-8">
              <h1 class="entry-title text-3xl lg:text-[42px] font-bold leading-tight mb-1"><?php the_title() ?></h1>
              <?php if ($short_description) : ?>
                <div class="text-stone-500 text-xl lg:text-2xl"><?php echo $short_description; ?></div>
              <?php endif ?>
            </header>
          </div>

        </div>
      </div>
    </div>

    <div class="bg-stone-800 lg:-mt-44 lg:pt-44 z-10">
      <div class="container mx-auto py-10 lg:py-24">
        <?php if ($description) : ?>
          <div class="product-description text-stone-200 text-base lg:text-xl max-w-prose mx-auto flex flex-col gap-y-6">
            <?php echo $description; ?>
          </div>
        <?php endif ?>
        <?php if ($brochure) : ?>
          <div class="mt-4 lg:mt-8 text-base lg:text-xl max-w-prose mx-auto flex flex-col gap-y-4">
            <?php foreach ($brochure as $row) : ?>
              <a href="<?php echo $row['download_file']['url'] ?>" target="_blank" download class="bg-black text-stone-200 p-4 rounded-lg lg:px-6 flex gap-x-2 lg:gap-x-4 hover:-translate-y-1 transition-all duration-200">
                <svg class="w-8 h-8 text-stone-300" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5 6V14.5H3.5V1.5H8V4.25C8 5.216 8.784 6 9.75 6H12.5ZM12.379 4.5L9.5 1.621V4.25C9.5 4.388 9.612 4.5 9.75 4.5H12.379ZM2 1C2 0.734784 2.10536 0.48043 2.29289 0.292893C2.48043 0.105357 2.73478 0 3 0L9.586 0C9.8512 5.66374e-05 10.1055 0.105451 10.293 0.293L13.707 3.707C13.8945 3.89449 13.9999 4.14881 14 4.414V15C14 15.2652 13.8946 15.5196 13.7071 15.7071C13.5196 15.8946 13.2652 16 13 16H3C2.73478 16 2.48043 15.8946 2.29289 15.7071C2.10536 15.5196 2 15.2652 2 15V1ZM5.75 8C5.55109 8 5.36032 8.07902 5.21967 8.21967C5.07902 8.36032 5 8.55109 5 8.75C5 8.94891 5.07902 9.13968 5.21967 9.28033C5.36032 9.42098 5.55109 9.5 5.75 9.5H10.25C10.4489 9.5 10.6397 9.42098 10.7803 9.28033C10.921 9.13968 11 8.94891 11 8.75C11 8.55109 10.921 8.36032 10.7803 8.21967C10.6397 8.07902 10.4489 8 10.25 8H5.75ZM5 11.25C5 11.0511 5.07902 10.8603 5.21967 10.7197C5.36032 10.579 5.55109 10.5 5.75 10.5H8.25C8.44891 10.5 8.63968 10.579 8.78033 10.7197C8.92098 10.8603 9 11.0511 9 11.25C9 11.4489 8.92098 11.6397 8.78033 11.7803C8.63968 11.921 8.44891 12 8.25 12H5.75C5.55109 12 5.36032 11.921 5.21967 11.7803C5.07902 11.6397 5 11.4489 5 11.25Z" fill="currentColor" />
                </svg>
                <h5 class="text-base lg:text-xl pt-1"><?php echo $row['brochure_title'] ?></h5>
              </a>
            <?php endforeach ?>
          </div>
        <?php endif ?>
      </div>
    </div>

    <?php if ($project_category) : ?>
      <div class="bg-stone-50 px-4 lg:-mb-20 lg:px-8 lg:pb-20">
        <div class="py-10 lg:py-28">

          <div class="flex flex-col md:flex-row">
            <div class="w-full">

              <?php
              $args = array(
                'post_type' => 'projects',
                'posts_per_page' => -1,
                'tax_query' => array(
                  array(
                    'taxonomy' => 'project_category',
                    'field'    => 'slug',
                    'terms'    => $project_category[0]->name,
                  ),
                ),
              );
              $posts_query = new WP_Query($args);

              if ($posts_query->have_posts()) {
                echo '<div id="projects-swiper" class="swiper">';
                echo '<div class="swiper-wrapper">';
                while ($posts_query->have_posts()) {
                  $posts_query->the_post();
                  $id = get_the_ID();
                  $url = get_the_permalink();
                  $image = get_the_post_thumbnail_url();
                  $title = get_the_title();
                  //$terms = get_the_terms($id, 'project_category');
                  //$subtitle = $terms[0]->name;
                  $subtitle = get_the_term_list($id, 'project_category', '', ', ');
                  $subtitle = strip_tags($subtitle);

                  echo '<div class="swiper-slide max-w-sm">';
                  echo '<a href="' . $url . '" class="block rounded-md overflow-hidden">';

                  echo '<div class="relative bg-stone-200 aspect-square">';
                  echo '<img class="absolute inset-0 w-full h-full object-cover" src="' . $image . '">';
                  echo '</div>';

                  //echo '<div class="absolute w-full h-full inset-0 bg-gradient-to-t from-stone-900 via-stone-900/25"></div>';
                  echo '<div class="rounded-b-md px-4 py-4 border-x border-b border-solid border-stone-200 bg-white">';

                  echo '<h4 class="text-base font-bold mb-1 truncate">' . $title . '</h4>';
                  echo '<p class="text-sm text-stone-600 truncate">' . $subtitle . '</p>';

                  echo '</div>';

                  echo '</a>';
                  echo '</div>';
                }
                echo '</div>';
                echo '<div class="swiper-pagination hidden lg:block"></div>';
                echo '<div class="swiper-buttons flex gap-x-2 z-20 justify-center md:justify-end mt-4 md:mr-8">';
                echo '<div class="swiper-button-prev"></div>';
                echo '<div class="swiper-button-next"></div>';
                echo '</div>';
                echo '</div>';
                echo '
          <script>
          var swiper = new Swiper("#projects-swiper", {
            slidesPerView: 2,
            spaceBetween: 16,
            slidesPerGroup: 1,
            loop: false,
            loopFillGroupWithBlank: true,
            grabCursor: true,
            pagination: {
              el: "#projects-swiper .swiper-pagination",
              clickable: false,
            },
            navigation: {
              nextEl: "#projects-swiper .swiper-button-next",
              prevEl: "#projects-swiper .swiper-button-prev",
            },
            breakpoints: {
              768: {
                slidesPerView: "auto",
                spaceBetween: 30
              },
              1024: {
                slidesPerView: 3,
                spaceBetween: 20
              },
              1200: {
                slidesPerView: 4,
                spaceBetween: 30
              },
            }
          });
        </script>
          ';
              } else {
              ?>
                <div class="text-lg md:text-center md:text-3xl">Sorry, there's no post in this category.</div>
              <?php
              }
              wp_reset_postdata();
              ?>


            </div>
          </div>
        </div>
      </div>
    <?php endif ?>

  <?php endwhile; ?>

<?php endif; ?>

<?php
get_footer();
