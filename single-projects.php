<?php get_header(); ?>

<?php if (have_posts()) : ?>

  <?php
  while (have_posts()) :
    the_post();

    $images = get_field('images');
    $description = get_field('description');
  ?>

    <div class="container mx-auto text-center pt-8 pb-4 lg:pt-16 lg:pb-12">
      <div class="text-stone-600 text-sm mb-4 xl:mb-8">
        <a href="/" class="hover:text-primary hover:underline"><span>Home</span></a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="/projects" class="hover:text-primary hover:underline">Our Projects</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<strong><?php the_title(); ?></strong>
      </div>
      <h1 class="text-4xl font-extrabold tracking-tight leading-snug mb-1 xl:text-[48px]"><?php the_title(); ?></h1>
      <div class="font-medium text-stone-500 uppercase text-sm xl:text-base">
        <?php
        echo get_the_term_list($post->ID, 'project_category', '', ', ');
        ?>
      </div>
    </div>

    <div class="container mx-auto">
      <?php
      if ($images) : ?>
        <div id="product-swiper" class="swiper rounded-xl">
          <div class="swiper-wrapper rounded-r-xl">
            <?php foreach ($images as $image) : ?>
              <div class="swiper-slide bg-slate-800 aspect-video">
                <div class="h-full w-full relative">
                  <?php
                  $caption = $image['caption'];
                  if ($caption) {
                    echo '<div class="hidden lg:block absolute bottom-16 left-[50%] -ml-[25%] w-1/2 bg-white bg-opacity-80 p-3 text-center">';
                    echo $caption;
                    echo '</div>';
                  }
                  ?>
                  <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="h-full object-contain mx-auto" />
                </div>
              </div>
              <li>
                <a href="<?php echo esc_url($image['url']); ?>">
                  <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                </a>
              </li>
            <?php endforeach; ?>
          </div>
          <div class="swiper-pagination"></div>
          <div class="swiper-buttons flex gap-x-1 absolute right-4 bottom-4 z-10 lg:gap-x-2">
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
          </div>
        </div>
        <script>
          // Product Swiper
          const productSwiper = new Swiper('#product-swiper', {
            direction: 'horizontal',
            loop: false,
            // autoplay: {
            //   delay: 5000,
            //   disableOnInteraction: false,
            // },
            pagination: {
              el: '.swiper-pagination',
              clickable: true,
            },
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
          });
        </script>
      <?php endif; ?>
    </div>
    <?php if ($description) : ?>
      <div class="container mx-auto pt-8 pb-12 xl:pt-16 xl:pb-24">
        <div class="text-xl max-w-prose mx-auto flex flex-col gap-y-6"><?php echo $description; ?></div>
      </div>
    <?php endif; ?>
    <?php
    $arrow_left = interdesign_icon(array(
      'icon'  => 'arrow-long',
      'group'  => 'content',
      'size'  => 20,
      'class'  => 'fill-blue-500 rotate-180 mr-2',
    ));
    $arrow_right = interdesign_icon(array(
      'icon'  => 'arrow-long',
      'group'  => 'content',
      'size'  => 20,
      'class'  => 'fill-blue-500 ml-2',
    ));
    ?>
    <div class="container mx-auto pt-8 pb-12 xl:pt-16 xl:pb-24">
      <div class="flex gap-4">
        <div class="previous-post-link text-left w-1/2">
          <?php previous_post_link($format = '%link', $link = $arrow_left . '<span>%title</span>', $in_same_term = true, $excluded_terms = '', $taxonomy = 'project_category'); ?>
        </div>

        <div class="next-post-link text-right w-1/2">
          <?php next_post_link($format = '%link', $link = '<span>%title</span>' . $arrow_right, $in_same_term = true, $excluded_terms = '', $taxonomy = 'project_category'); ?>
        </div>
      </div>
    </div>


  <?php endwhile; ?>

<?php endif; ?>

<?php
get_footer();
