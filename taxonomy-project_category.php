<?php get_header(); ?>

<?php
$term = get_queried_object();
?>
<div class="container mx-auto pb-10 lg:pb-28">
  <div class="pt-8 pb-4 lg:pt-16 lg:pb-20">
    <div class="text-center max-w-lg mx-auto">
      <div class="text-stone-600 text-sm mb-4 xl:mb-8">
        <a href="/" class="hover:text-primary hover:underline"><span>Home</span></a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<a href="/projects" class="hover:text-primary hover:underline">Our Projects</a>&nbsp;&nbsp;&raquo;&nbsp;&nbsp;<strong><?php echo $term->name ?></strong>
      </div>
      <h1 class="text-[48px] font-extrabold tracking-tight"><?php echo $term->name ?></h1>
    </div>
  </div>

  <?php
  $args = array(
    'posts_per_page'      => -1,
    'post_type'     => 'projects',
    'tax_query' => array(
      array(
        'taxonomy' => 'project_category',
        'field'    => 'slug',
        'terms'    => $term->slug,
      ),
    ),
    'orderby' => 'title',
    'order'    => 'ASC',
    'post_status' => 'publish'
  );

  query_posts($args);
  ?>
  <?php
  if (have_posts()) : ?>
    <div class="max-w-5xl mx-auto">
      <div class="grid grid-cols-2 gap-2 lg:grid-cols-3 lg:gap-8">
        <?php
        while (have_posts()) :
          the_post();
        ?>

          <?php get_template_part('template-parts/project-grid-item', get_post_format()); ?>

        <?php endwhile; ?>
      </div>
    </div>
  <?php
  endif;
  wp_reset_query();
  ?>

</div>

<?php
get_footer();
