<?php

/**
 * Template Name: Homepage
 */

get_header();

function highlight_icon($icon, $title, $subtitle, $class)
{
  $output = '<div class="flex flex-col text-center md:text-left md:flex-row ' . $class . '">';
  $output .= '<div class="md:mr-3">';
  $output .= interdesign_icon(array(
    'icon'  => $icon,
    'group'  => 'content',
    'size'  => 42,
    'class'  => 'mx-auto md:mx-0',
  ));
  $output .= '</div>';
  $output .= '<div class="w-full">';
  $output .= '<h4 class="text-lg font-bold lg:text-xl">' . $title . '</h4>';
  $output .= '<p class="text-stone-500 text-xs">' . $subtitle . '</p>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

?>

<div class="container mx-auto">

  <div class="relative xl:-mx-4 2xl:-mx-16">
    <div id="home-swiper" class="swiper rounded-xl">
      <div class="swiper-wrapper rounded-xl">
        <div class="swiper-slide bg-slate-200 aspect-square md:aspect-video">
          <img src="/wp-content/uploads/2021/12/homeslide-01.jpg" class="h-full w-full object-cover" alt="" />
        </div>
        <div class="swiper-slide bg-slate-200 aspect-square md:aspect-video">
          <img src="/wp-content/uploads/2021/12/homeslide-02.jpg" class="h-full w-full object-cover" alt="" />
        </div>
        <div class="swiper-slide bg-slate-200 aspect-square md:aspect-video">
          <img src="/wp-content/uploads/2021/12/homeslide-03.jpg" class="h-full w-full object-cover" alt="" />
        </div>
        <div class="swiper-slide bg-slate-200 aspect-square md:aspect-video">
          <img src="/wp-content/uploads/2021/12/homeslide-04.jpg" class="h-full w-full object-cover" alt="" />
        </div>
      </div>
      <div class="swiper-pagination"></div>
      <div class="swiper-buttons flex gap-x-1 absolute z-30 right-4 top-4 lg:gap-x-2 lg:top-auto lg:right-6 lg:bottom-6">
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    </div>
    <div class="absolute bottom-4 left-4 rounded-md z-10 md:left-8 md:bottom-12 lg:py-10 lg:px-8 xl:left-16 xl:bottom-16">
      <h2 class="text-3xl text-white leading-[1.1em] font-extrabold tracking-tight mb-4 max-w-sm md:text-4xl xl:text-5xl xl:max-w-2xl">We Serve The Best Architectural Product</h2>
      <a href="/products-services" class="inline-block px-4 py-3 bg-blue-500 uppercase text-center rounded text-white text-sm font-semibold transition duration-300 hover:bg-blue-400">Our Products & Services</a>
    </div>
    <script>
      // Homepage Swiper
      const homeSwiper = new Swiper('#home-swiper', {
        direction: 'horizontal',
        loop: true,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        pagination: {
          el: '#home-swiper .swiper-pagination',
        },
        navigation: {
          nextEl: '#home-swiper .swiper-button-next',
          prevEl: '#home-swiper .swiper-button-prev',
        },
      });
    </script>
  </div>

</div>

<div class="container mx-auto">
  <div class="grid grid-cols-2 gap-8 py-10 md:grid-cols-3 xl:grid-cols-5 xl:gap-6 xl:py-20">
    <?php echo highlight_icon('highquality', 'Quality', 'Best Products & Services', ''); ?>
    <?php echo highlight_icon('experienced', 'Commitment', 'Timely Schedule & Quality', ''); ?>
    <?php echo highlight_icon('satisfaction', 'Satisfaction', 'Outstanding Services', ''); ?>
    <?php echo highlight_icon('excellence', 'Teamwork', 'Achieve Goals Together', ''); ?>
    <?php echo highlight_icon('innovative', 'Creativity', 'Never Ending Innovation', ''); ?>
  </div>
</div>

<div class="bg-stone-100 py-10 md:py-12 lg:py-16 xl:py-28">
  <div class="container mx-auto">
    <div class="flex justify-center pt-8 pb-4 lg:pb-8">
      <img class="max-h-[48px] w-auto md:max-h-[56px] xl:max-h-[80px]" src="<?php echo get_stylesheet_directory_uri() . '/images/logo-interdesign.svg'; ?>" />
    </div>
    <div class="pb-8 lg:pb-16">
      <h2 class="text-3xl font-extrabold tracking-tight text-center md:text-4xl xl:text-[46px]">Products & Services</h2>
    </div>
    <?php echo interdesign_products_grid_2(); ?>
  </div>
</div>


<div class="bg-white px-4 lg:-mb-20 lg:pb-20">
  <div class="py-10 lg:py-32">
    <div class="flex flex-col md:flex-row">
      <div class="w-full md:w-2/5 lg:w-1/3 md:max-w-md xl:ml-20">
        <h2 class="w-full text-3xl mb-4 md:text-4xl md:mt-8 lg:mb-6 lg:text-[42px] font-extrabold leading-[1.1em]">Discover Our Latest News & Projects</h2>
        <a href="/projects" class="hidden md:inline-block px-4 py-3 whitespace-nowrap bg-blue-500 uppercase text-center rounded text-white text-sm font-semibold transition duration-300 hover:bg-blue-400">More Projects</a>
      </div>
      <div class="w-full md:w-3/5 lg:w-2/3 md:pl-20">

        <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
          'post_type' => 'projects',
          'posts_per_page' => 8,
          'paged' => $paged,
        );
        $posts_query = new WP_Query($args);

        if ($posts_query->have_posts()) {
          echo '<div id="projects-swiper" class="swiper">';
          echo '<div class="swiper-wrapper">';
          while ($posts_query->have_posts()) {
            $posts_query->the_post();
            $id = get_the_ID();
            $url = get_the_permalink();
            $image = get_the_post_thumbnail_url();
            $title = get_the_title();
            $terms = get_the_terms($id, 'project_category');
            $subtitle = $terms[0]->name;

            echo '<div class="swiper-slide max-w-sm">';
            echo '<a href="' . $url . '" class="block rounded-md overflow-hidden">';

            echo '<div class="relative bg-stone-200 aspect-square">';
            echo '<img class="absolute inset-0 w-full h-full object-cover" src="' . $image . '">';
            echo '</div>';

            //echo '<div class="absolute w-full h-full inset-0 bg-gradient-to-t from-stone-900 via-stone-900/25"></div>';
            echo '<div class="rounded-b-md px-4 py-4 border-x border-b border-solid border-stone-200 bg-white">';

            echo '<h4 class="text-base font-bold mb-1 truncate">' . $title . '</h4>';
            echo '<p class="text-sm text-stone-600">' . $subtitle . '</p>';

            echo '</div>';

            echo '</a>';
            echo '</div>';
          }
          echo '</div>';
          echo '<div class="swiper-pagination hidden lg:block"></div>';
          echo '<div class="swiper-buttons flex gap-x-2 z-20 justify-center md:justify-end mt-4 md:mr-8">';
          echo '<div class="swiper-button-prev"></div>';
          echo '<div class="swiper-button-next"></div>';
          echo '</div>';
          echo '</div>';
          echo '
          <script>
          var swiper = new Swiper("#projects-swiper", {
            slidesPerView: 2,
            spaceBetween: 16,
            slidesPerGroup: 1,
            loop: false,
            loopFillGroupWithBlank: true,
            grabCursor: true,
            pagination: {
              el: "#projects-swiper .swiper-pagination",
              clickable: false,
            },
            navigation: {
              nextEl: "#projects-swiper .swiper-button-next",
              prevEl: "#projects-swiper .swiper-button-prev",
            },
            breakpoints: {
              768: {
                slidesPerView: "auto",
                spaceBetween: 30
              },
              1024: {
                slidesPerView: 2,
                spaceBetween: 20
              },
              1200: {
                slidesPerView: 3,
                spaceBetween: 30
              },
            }
          });
        </script>
          ';
        } else {
        ?>
          <div class="text-center text-3xl">Sorry, there's no post in this category.</div>
        <?php
        }
        wp_reset_postdata();
        ?>


      </div>
    </div>
    <div class="flex justify-center mt-8 md:hidden">
      <a href="/projects" class="inline-block px-4 py-3 whitespace-nowrap bg-blue-500 uppercase text-center rounded text-white text-sm font-semibold transition duration-300 hover:bg-blue-400">More Projects</a>
    </div>
  </div>
</div>

<?php get_footer(); ?>