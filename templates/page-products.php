<?php

/**
 * Template Name: Products
 */

get_header();
?>
<div class="container mx-auto pb-28">
  <div class="pt-8 pb-4 lg:pt-16 lg:pb-20">
    <div class="text-center max-w-prose mx-auto">
      <h2 class="text-[48px] font-extrabold tracking-tight">Our Products & Services</h2>
    </div>
  </div>
  <div class="flex flex-wrap lg:flex-nowrap">
    <div class="w-full mb-8 hidden lg:block lg:w-1/5 lg:pr-10">
      <div class="">
        <?php
        $args = array(
          'post_type'         => array('products-services'),
          'post_status'       => array('publish'),
          'post_parent'       => 0,
          'posts_per_page'    => '-1',
          'order'             => 'ASC',
          'orderby'           => 'menu_order',
        );
        $query = new WP_Query($args);

        if ($query->have_posts()) {
          echo '<h4 class="mb-6 lg:mb-8 font-light text-stone-500">BROWSE</h4>';
          echo '<ul class="flex flex-col gap-y-4 text-sm">';
          while ($query->have_posts()) {
            $query->the_post();

            echo '<li>';
            echo '<a class="font-semibold text-left hover:text-primary" href="' . get_the_permalink() . '">' . get_the_title() . '</a>';

            $sub_pages_args = array(
              'post_type' => 'products-services',
              'child_of' => get_the_ID(),
              'order'             => 'ASC',
              'sort_column'           => 'menu_order',
            );

            $sub_pages = get_pages($sub_pages_args);

            // echo '<pre>';
            // print_r($sub_pages);
            // echo '</pre>';

            if ($sub_pages) {
              echo '<ul class="flex flex-col gap-y-2 pt-3 pl-2 pb-2">';
              foreach ($sub_pages as $sub_page) {
                echo '<li>';
                echo '<a class="text-left hover:text-primary" href="' . get_the_permalink($sub_page->ID) . '">' . $sub_page->post_title . '</a>';
                echo "</li>";
              }
              echo "</ul>";
            }

            echo '</li>';
          }
          echo '</ul>';
        }

        // echo '<pre>';
        // print_r($query);
        // echo '</pre>';

        wp_reset_postdata();
        ?>
      </div>
    </div>
    <div class="w-full lg:w-4/5"><?php echo interdesign_products_grid_3() ?></div>
  </div>
</div>

<?php get_footer(); ?>