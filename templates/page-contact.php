<?php

/**
 * Template Name: Contact
 */

get_header();
$contact_title = get_field('contact_title');
$contact_text = get_field('contact_text');
$contact_form_shortcode = get_field('contact_form_shortcode');
?>

<div class="container mx-auto mt-4 mb-8 md:my-8">

  <header>
    <div class="flex flex-wrap xl:gap-12">
      <div class="w-full md:w-1/2">
        <h1 class="text-4xl md:text-6xl font-bold py-6 md:py-12"><?php echo $contact_title ?></h1>
      </div>
    </div>
  </header>

  <div class="flex flex-wrap lg:flex-nowrap">
    <div class="w-full lg:w-2/3">
      <div class="mb-10 prose lg:prose-lg xl:prose-xl">
        <?php
        if ($contact_text) {
          echo $contact_text;
        }
        ?>
        <?php
        if ($contact_form_shortcode) {
          echo '<div class="contact-form">';
          echo do_shortcode($contact_form_shortcode);
          echo '</div>';
        }
        ?>
      </div>
    </div>
    <div class="w-full lg:w-1/3 xl:pl-20">
      <div>
        <h4 class="text-stone-900 font-bold mb-8 text-lg">HEAD OFFICE</h4>
        <ul class="flex flex-col gap-y-4">
          <?php
          $email = get_field('email', 'option');
          $address = get_field('address', 'option');
          $google_map_link = get_field('google_map_link', 'option');
          $phone = get_field('phone', 'option');
          $fax = get_field('fax', 'option');
          $whatsapp_01 = get_field('whatsapp_01', 'option');
          $whatsapp_02 = get_field('whatsapp_02', 'option');
          ?>
          <?php if ($whatsapp_01 || $whatsapp_02) { ?>
            <li class="flex">
              <div class="flex-none mr-3 pt-0.5">
                <?php echo interdesign_icon(array(
                  'icon'  => 'whatsapp',
                  'group'  => 'menu',
                  'size'  => 20,
                  'class'  => 'fill-stone-500',
                )); ?>
              </div>
              <?php
              $wa_message = 'Halo%2C%20Saya%20mau%20minta%20info%20tentang%20produk%20dan%20layanan%20Interdesign.'
              ?>
              <div>
                <?php if ($whatsapp_01) { ?>
                  <?php
                  $whatsapp_01_replace = preg_replace('/^0/', '62', $whatsapp_01);
                  $whatsapp_01_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $whatsapp_01_replace);
                  ?>
                  <a href="https://wa.me/<?php echo $whatsapp_01_formatted ?>?text=<?php echo $wa_message ?>" target="_blank" class="hover:underline"><?php echo $whatsapp_01 ?></a>
                  <br />
                <?php } ?>
                <?php if ($whatsapp_02) { ?>
                  <?php
                  $whatsapp_02_replace = preg_replace('/^0/', '62', $whatsapp_02);
                  $whatsapp_02_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $whatsapp_02_replace);
                  ?>
                  <a href="https://wa.me/<?php echo $whatsapp_02_formatted ?>?text=<?php echo $wa_message ?>" target="_blank" class="hover:underline"><?php echo $whatsapp_02 ?></a>
                <?php } ?>
              </div>
            </li>
          <?php } ?>
          <?php if ($address) { ?>
            <li class="flex">
              <div class="flex-none mr-3 pt-0.5">
                <?php echo interdesign_icon(array(
                  'icon'  => 'location',
                  'group'  => 'menu',
                  'size'  => 20,
                  'class'  => 'fill-stone-500',
                )); ?>
              </div>
              <div>
                <?php echo $address ?>
                <br />
                <?php if ($google_map_link) { ?>
                  <a href="<?php echo $google_map_link ?>" target="_blank" class="text-blue-500 hover:underline">Google Map</a>
                <?php } ?>
              </div>
            </li>
          <?php } ?>
          <?php if ($email) { ?>
            <li class="flex">
              <div class="flex-none mr-3 pt-0.5">
                <?php echo interdesign_icon(array(
                  'icon'  => 'email',
                  'group'  => 'menu',
                  'size'  => 20,
                  'class'  => 'fill-stone-500',
                )); ?>
              </div>
              <div><a href="mailto:<?php echo $email ?>" target="_blank" class="hover:underline"><?php echo $email ?></a></div>
            </li>
          <?php } ?>
          <?php if ($phone) { ?>
            <li class="flex">
              <div class="flex-none mr-3 pt-0.5">
                <?php echo interdesign_icon(array(
                  'icon'  => 'contact',
                  'group'  => 'menu',
                  'size'  => 20,
                  'class'  => 'fill-stone-500',
                )); ?>
              </div>
              <?php
              $phone_replace = str_replace('(0', '62', $phone);
              $phone_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $phone_replace);
              ?>
              <div><a href="tel:<?php echo $phone_formatted ?>" target="_blank" class="hover:underline"><?php echo $phone ?></a></div>
            </li>
          <?php } ?>
          <?php if ($fax) { ?>
            <li class="flex">
              <div class="flex-none mr-3 pt-0.5">
                <?php echo interdesign_icon(array(
                  'icon'  => 'fax',
                  'group'  => 'menu',
                  'size'  => 20,
                  'class'  => 'fill-stone-500',
                )); ?>
              </div>
              <?php
              $fax_replace = str_replace('(0', '62', $fax);
              $fax_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $fax_replace);
              ?>
              <div><a href="tel:<?php echo $fax_formatted ?>" target="_blank" class="hover:underline"><?php echo $fax ?></a></div>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>

  <div class="grid grid-cols-1 gap-4 mt-4 mb-4 md:grid-cols-3 lg:gap-8 lg:mt-8 lg:mb-16 text-sm">
    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>Tangerang office / warehouse</strong></li>
        <li>
          <div>
            Jl. Baetussa'adah no. 2.<br />
            Kadu wetan. Curug bitung.<br />
            Tangerang 15810
          </div>
        </li>
      </ul>
    </div>
    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>BEKASI OFFICE / WAREHOUSE</strong></li>
        <li>
          <div>
            Jl. Kaliabang nangka no 3.<br />Rt 001 / Rw 007, Harapan Jaya,<br />Bekasi Utara 17124
          </div>
        </li>
      </ul>
    </div>
    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>Surabaya</strong></li>
        <li>
          <div class="mb-3">
            Jl. Raya Darmo Permai 1 No. 5<br />
            Pradah Kali Kendal, Dukuh Pakis,<br />
            Kota Surabaya Jawa Timur
          </div>
          <div>
            Jl. Margomulyo Permai L Bo.1<br />
            Tambak Sariksi, Asemrowo<br />
            Kota Surabaya, Jawa Timur
          </div>
        </li>
      </ul>
    </div>

    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>Medan</strong></li>
        <li>
          <div>
            Komplek Amplas Trade Center Blok D no18-19<br />Jl. Sisingamangaraja Km 10<br />Medan-Amplas
          </div>
        </li>
      </ul>
    </div>

    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>Pekan Baru</strong></li>
        <li>
          <div class="mb-3">
            Komplek Pergudangan Angkasa 1 Blok B5<br />Jl. SM Amin, Simalang Baru, Tampan<br />Pekanbaru
          </div>
          <div>
            Komplek Pergudangan Angkasa 1 Blok A12<br />Jl. SM Amin, Simalang Baru, Tampan<br />Pekanbaru
          </div>
        </li>
      </ul>
    </div>

    <div>
      <ul class="flex flex-col gap-y-2">
        <li class="uppercase"><strong>Batam</strong></li>
        <li>
          <div>
            Komplek Puri Industrial Park 2000<br />Blok B No.7<br />Batam Center
          </div>
        </li>
      </ul>
    </div>

  </div>

</div>

<?php get_footer(); ?>