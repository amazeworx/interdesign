<?php

/**
 * Template Name: Projects
 */

get_header();
// Gets Categories by Post Type
// returns categories that aren't empty
function get_categories_for_post_type($post_type = 'post', $taxonomy = '')
{
  $exclude = array();
  $args = array(
    "taxonomy" => $taxonomy,
    'hide_empty'      => false,
  );
  $categories = get_categories($args);

  // Check ALL categories for posts of given post type
  foreach ($categories as $category) {
    $posts = get_posts(array(
      'post_type' => $post_type,
      'tax_query' => array(
        array(
          'taxonomy' => $taxonomy,
          'field' => 'term_id',
          'terms' => $category->term_id
        )
      )
    ));

    // If no posts in category, add to exclude list
    // if (empty($posts)) {
    //   $exclude[] = $category->term_id;
    // }
  }

  // If exclude list, add to args
  //if (!empty($exclude)) {
  //$args['exclude'] = implode(',', $exclude);
  //}

  // List categories
  return get_categories($args);
}

function get_taxonomy_hierarchy($taxonomy, $parent = 0)
{
  // only 1 taxonomy
  $taxonomy = is_array($taxonomy) ? array_shift($taxonomy) : $taxonomy;
  // get all direct descendants of the $parent
  $terms = get_terms($taxonomy, array(
    'parent' => $parent,
    'hide_empty'      => false,
  ));
  // prepare a new array.  these are the children of $parent
  // we'll ultimately copy all the $terms into this new array, but only after they
  // find their own children
  $children = array();
  // go through all the direct descendants of $parent, and gather their children
  foreach ($terms as $term) {
    // recurse to get the direct descendants of "this" term
    $term->children = get_taxonomy_hierarchy($taxonomy, $term->term_id);
    // add the term to our new array
    $children[$term->term_id] = $term;
  }
  // send the results back to the caller
  return $children;
}

function project_item($url, $image, $title, $subtitle)
{
  $output = '<a href="' . $url . '" class="relative flex flex-col justify-center rounded-md overflow-hidden bg-stone-900 aspect-square transition duration-500 ease-in-out hover:lg:shadow-xl hover:lg:shadow-stone-900/50 hover:lg:-translate-y-1">';

  if ($image) {
    $output .= '<img class="absolute inset-0 w-full h-full object-cover z-0" src="' . $image . '">';
  }

  $output .= '<div class="absolute z-10 w-full h-full inset-0 bg-stone-900/40"></div>';
  $output .= '<div class="text-white p-4 relative z-20 lg:p-8">';
  $output .= '<h4 class="text-base leading-tight font-bold text-center uppercase lg:text-xl lg:mb-1 xl:text-2xl">' . $title . '</h4>';
  //$output .= '<p class="text-xs lg:text-sm xl:text-base">' . $subtitle . '</p>';
  $output .= '</div>';

  $output .= '</a>';

  return $output;
}

?>
<div class="container mx-auto pb-10 lg:pb-28">
  <div class="pt-8 pb-4 lg:pt-16 lg:pb-16">
    <div class="text-center max-w-lg mx-auto">
      <h2 class="text-[48px] font-extrabold tracking-tight">Our Projects</h2>
      <p class="text-stone-500 font-semibold text-base lg:text-lg">For more than 30 years, we are experienced in providing solutions for any kind of construction projects. </p>
    </div>
  </div>
  <div class="flex flex-wrap">
    <div class="w-full">
      <div id="projects-grid" class="-mx-2 md:max-w-5xl md:mx-auto">
        <?php
        $args = array(
          "taxonomy" => 'project_category',
          "parent" => 0,
          'orderby' => 'menu_order',
          'order'   => 'ASC',
          'hide_empty'      => false,
        );
        $categories = get_categories($args);

        if ($categories) {
          echo '<div class="grid grid-cols-2 gap-2 lg:grid-cols-3 lg:gap-8">';

          foreach ($categories as $category) {

            $id = $category->term_id;
            $url = get_term_link($id);
            $image = get_field('featured_image', 'term_' . $id)['url'];
            //echo $image;
            //print_r($image);
            $title = $category->name;
            $subtitle = $category->description;;

            echo project_item($url, $image, $title, $subtitle);
          }

          echo '</div>';
        }

        ?>
      </div>
    </div>

  </div>
</div>

</div>

<?php get_footer(); ?>