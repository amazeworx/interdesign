<?php

/**
 * Template Name: Profile
 */

get_header();
$page_title = get_field('page_title');
$slide_images = get_field('slide_images');
$content_sections = get_field('content_sections');
?>

<div class="container mx-auto my-8">

  <header>
    <div class="flex flex-wrap">
      <div class="w-full lg:w-1/2">
        <h1 class="text-4xl lg:text-6xl font-bold py-6 lg:py-12"><?php echo $page_title ?></h1>
      </div>
    </div>
  </header>

  <div class="-mx-4 mb-6 lg:-mx-16 lg:mb-16">
    <div id="profile-swiper" class="swiper lg:rounded-xl">
      <div class="swiper-wrapper lg:rounded-xl">
        <?php foreach ($slide_images as $image) : ?>
          <div class="swiper-slide bg-slate-200 aspect-video lg:aspect-[16/8]">
            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="h-full w-full object-cover" />
          </div>
          <li>
            <a href="<?php echo esc_url($image['url']); ?>">
              <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            </a>
          </li>
        <?php endforeach; ?>
      </div>
      <div class="swiper-pagination"></div>
      <div class="swiper-buttons flex gap-x-2 absolute right-6 bottom-6 z-30">
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
      </div>
    </div>
    <script>
      const profileSwiper = new Swiper('#profile-swiper', {
        direction: 'horizontal',
        loop: true,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false,
        },
        pagination: {
          el: '#profile-swiper .swiper-pagination',
        },
        navigation: {
          nextEl: '#profile-swiper .swiper-button-next',
          prevEl: '#profile-swiper .swiper-button-prev',
        },
      });
    </script>
  </div>

  <?php
  if (have_rows('content_sections')) :

    echo '<div class="relative flex flex-wrap xl:mb-32">';

    echo '<nav id="scrollnav" class="hidden w-full md:w-1/4 xl:w-1/3 xl:pr-16 md:block md:sticky md:top-6 self-start">';
    echo '<ul class="p-8 rounded-md shadow-lg">';
    while (have_rows('content_sections')) : the_row();
      $section_title = get_sub_field('section_title');
      echo '<li><a href="#' . interdesign_create_slug($section_title) . '" class="block py-2 xl:text-lg text-stone-500 hover:text-primary">' . $section_title . '</a></li>';
    endwhile;
    echo '</ul>';
    echo '</nav>';


    echo '<div class="w-full md:w-2/4 xl:w-2/3">';
    echo '<article class="">';
    while (have_rows('content_sections')) : the_row();
      $section_title = get_sub_field('section_title');
      $section_content = get_sub_field('section_content');
      echo '<section id="' . interdesign_create_slug($section_title) . '" class="mb-8 xl:mb-16 prose prose-stone xl:prose-xl prose-h2:font-light xl:prose-h2:text-[2.5rem]">';
      echo '<h2 class="scroll-mt-6">' . $section_title . '</h2>';
      echo $section_content;
      echo '</section>';
    endwhile;
    echo '</article>';
    echo '</div>';

    echo '</div>';

  endif;
  ?>


</div>

<?php get_footer(); ?>