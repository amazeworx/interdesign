const _ = require("lodash");
const theme = require('./theme.json');
const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

module.exports = {
  mode: 'jit',
  content: [
    './*/*.php',
    './**/*.php',
    './resources/css/*.css',
    './resources/js/*.js',
    './safelist.txt',
    './node_modules/tw-elements/dist/js/**/*.js'
  ],
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        md: '1.5rem',
        lg: '2rem'
      },
    },
    extend: {
      colors: tailpress.colorMapper(tailpress.theme('settings.color.palette', theme))
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px'
    }
  },
  plugins: [
    tailpress.tailwind,
    require('@tailwindcss/typography'),
    require('tw-elements/dist/plugin')
  ]
};
