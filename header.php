<!DOCTYPE html>
<html <?php language_attributes(); ?> class="scroll-smooth">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <?php wp_head(); ?>
</head>

<body <?php body_class('bg-white text-stone-900'); ?>>

  <?php do_action('interdesign_site_before'); ?>

  <div id="page" class="min-h-screen flex flex-col">

    <?php do_action('interdesign_header'); ?>

    <header>

      <div class="mx-auto container">
        <div class="flex items-center py-6 lg:py-10 lg:justify-between">
          <div class="logo-container relative z-0 mr-auto xl:mr-0">
            <?php if (has_custom_logo()) { ?>
              <?php the_custom_logo(); ?>
            <?php } else { ?>
              <div class="text-lg uppercase">
                <a href="<?php echo get_bloginfo('url'); ?>" class="font-extrabold text-lg uppercase">
                  <?php echo get_bloginfo('name'); ?>
                </a>
              </div>

              <p class="text-sm font-light text-gray-600">
                <?php echo get_bloginfo('description'); ?>
              </p>

            <?php } ?>
          </div>

          <div id="slide-over" class="fixed inset-0 -z-10 xl:relative xl:inset-auto xl:z-0">
            <div id="menu-overlay" class="absolute inset-0 bg-stone-900 bg-opacity-75 transition-opacity ease-in-out duration-500 opacity-0" aria-hidden="true"></div>
            <div id="main-menu" class="bg-white pt-10 pb-4 px-4 min-w-[256px] fixed z-30 inset-y-0 right-0 transform transition ease-in-out duration-500 translate-x-full xl:relative xl:inset-auto xl:p-0 xl:block xl:translate-x-0">
              <button type="button" href="#" aria-label="Toggle navigation" id="main-menu-close" class="inline-block absolute right-4 top-4 xl:hidden">
                <span class="sr-only">Close panel</span>
                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
              <?php
              wp_nav_menu(
                array(
                  'container_id'    => 'primary-menu',
                  'container_class' => 'font-semibold py-4 xl:p-0',
                  'menu_class'      => 'xl:flex xl:-mx-5',
                  'theme_location'  => 'primary',
                  'li_class'        => 'py-2 xl:py-0 xl:mx-5',
                  'link_before' => '<span>',
                  'link_after' => '</span>',
                  'fallback_cb'     => false,
                )
              );
              ?>
            </div>
          </div>

          <?php
          $instagram_url = get_field('instagram_url', 'option');
          $facebook_url = get_field('facebook_url', 'option');
          $linked_in_url = get_field('linked_in_url', 'option');
          if ($instagram_url || $facebook_url || $linked_in_url) {
          ?>
            <div class="hidden md:block">
              <ul class="flex gap-3">
                <?php
                if ($instagram_url) {
                  echo '<li><a class="block" href="' . $instagram_url . '" target="_blank">'
                    . interdesign_icon(array('group' => 'social', 'icon' => 'instagram', 'title' => 'Instagram', 'size' => '28', 'class' => 'h-6 w-6 transition duration-300 fill-stone-900 hover:fill-stone-500 xl:h-[28px] xl:w-[28px]')) . '</a></li>';
                }
                if ($facebook_url) {
                  echo '<li><a class="block" href="' . $facebook_url . '" target="_blank">'
                    . interdesign_icon(array('group' => 'social', 'icon' => 'facebook', 'title' => 'Facebook', 'size' => '28', 'class' => 'h-6 w-6 transition duration-300 fill-stone-900 hover:fill-stone-500 xl:h-[28px] xl:w-[28px]')) . '</a></li>';
                }
                if ($linked_in_url) {
                  echo '<li><a class="block" href="' . $linked_in_url . '" target="_blank">'
                    . interdesign_icon(array('group' => 'social', 'icon' => 'linkedin', 'title' => 'Linked In', 'size' => '28', 'class' => 'h-6 w-6 transition duration-300 fill-stone-900 hover:fill-stone-500 xl:h-[28px] xl:w-[28px]')) . '</a></li>';
                }
                ?>
              </ul>
            </div>
          <?php } ?>

          <div class="xl:hidden ml-4 relative">
            <button type="button" aria-label="Toggle navigation" id="main-menu-toggle" class="inline-flex p-1">
              <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
            </button>
          </div>

        </div>
      </div>
    </header>

    <div id="content" class="site-content flex-grow relative z-0">

      <?php do_action('interdesign_content_start'); ?>

      <main>