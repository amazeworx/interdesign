//import 'tw-elements';

// Scroll Spy
// https://github.com/r3plica/Scrollspy
!(function ($, window, document, undefined) {
  $.fn.extend({
    scrollspy: function (options) {
      var defaults = {
        namespace: 'scrollspy',
        activeClass: 'active',
        animate: !1,
        duration: 1e3,
        offset: 0,
        container: window,
        replaceState: !1,
      };
      options = $.extend({}, defaults, options);
      var add = function (ex1, ex2) {
          return parseInt(ex1, 10) + parseInt(ex2, 10);
        },
        findElements = function (links) {
          for (var elements = [], i = 0; i < links.length; i++) {
            var link = links[i],
              hash = $(link).attr('href'),
              element = $(hash);
            if (element.length > 0) {
              var top = Math.floor(element.offset().top),
                bottom = top + Math.floor(element.outerHeight());
              elements.push({
                element: element,
                hash: hash,
                top: top,
                bottom: bottom,
              });
            }
          }
          return elements;
        },
        findLink = function (links, hash) {
          for (var i = 0; i < links.length; i++) {
            var link = $(links[i]);
            if (link.attr('href') === hash) return link;
          }
        },
        resetClasses = function (links) {
          for (var i = 0; i < links.length; i++)
            $(links[i]).parent().removeClass(options.activeClass);
        },
        scrollArea = '';
      return this.each(function () {
        for (
          var element = this,
            container = $(options.container),
            links = $(element).find('a'),
            i = 0;
          i < links.length;
          i++
        ) {
          var link = links[i];
          $(link).on('click', function (e) {
            var target = $(this).attr('href'),
              $target = $(target);
            if ($target.length > 0) {
              var top = add($target.offset().top, options.offset);
              options.animate
                ? $('html, body').animate({ scrollTop: top }, options.duration)
                : window.scrollTo(0, top),
                e.preventDefault();
            }
          });
        }
        resetClasses(links);
        var elements = findElements(links),
          trackChanged = function () {
            for (
              var link,
                position = {
                  top: add($(this).scrollTop(), Math.abs(options.offset)),
                  left: $(this).scrollLeft(),
                },
                i = 0;
              i < elements.length;
              i++
            ) {
              var current = elements[i];
              if (
                position.top >= current.top &&
                position.top < current.bottom
              ) {
                var hash = current.hash;
                if ((link = findLink(links, hash))) {
                  options.onChange &&
                    scrollArea !== hash &&
                    (options.onChange(current.element, $(element), position),
                    (scrollArea = hash)),
                    options.replaceState &&
                      history.replaceState({}, '', '/' + hash),
                    resetClasses(links),
                    link.parent().addClass(options.activeClass);
                  break;
                }
              }
            }
            !link &&
              'exit' !== scrollArea &&
              options.onExit &&
              (options.onExit($(element), position),
              resetClasses(links),
              (scrollArea = 'exit'),
              options.replaceState && history.replaceState({}, '', '/'));
          };
        container.bind('scroll.' + options.namespace, function () {
          trackChanged();
        }),
          $(document).ready(function (e) {
            trackChanged();
          });
      });
    },
  });
})(jQuery, window, document);

/*!
 * FitVids 1.1
 *
 * Copyright 2013, Chris Coyier - http://css-tricks.com + Dave Rupert - http://daverupert.com
 * Credit to Thierry Koblentz - http://www.alistapart.com/articles/creating-intrinsic-ratios-for-video/
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 *
 */
(function ($) {
  'use strict';

  $.fn.fitVids = function (options) {
    var settings = {
      customSelector: null,
      ignore: null,
    };

    if (!document.getElementById('fit-vids-style')) {
      // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
      var head = document.head || document.getElementsByTagName('head')[0];
      var css =
        '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
      var div = document.createElement('div');
      div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
      head.appendChild(div.childNodes[1]);
    }

    if (options) {
      $.extend(settings, options);
    }

    return this.each(function () {
      var selectors = [
        'iframe[src*="player.vimeo.com"]',
        'iframe[src*="youtube.com"]',
        'iframe[src*="youtube-nocookie.com"]',
        'iframe[src*="kickstarter.com"][src*="video.html"]',
        'object',
        'embed',
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var ignoreList = '.fitvidsignore';

      if (settings.ignore) {
        ignoreList = ignoreList + ', ' + settings.ignore;
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not('object object'); // SwfObj conflict patch
      $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

      $allVideos.each(function () {
        var $this = $(this);
        if ($this.parents(ignoreList).length > 0) {
          return; // Disable FitVids on this video.
        }
        if (
          (this.tagName.toLowerCase() === 'embed' &&
            $this.parent('object').length) ||
          $this.parent('.fluid-width-video-wrapper').length
        ) {
          return;
        }
        if (
          !$this.css('height') &&
          !$this.css('width') &&
          (isNaN($this.attr('height')) || isNaN($this.attr('width')))
        ) {
          $this.attr('height', 9);
          $this.attr('width', 16);
        }
        var height =
            this.tagName.toLowerCase() === 'object' ||
            ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10)))
              ? parseInt($this.attr('height'), 10)
              : $this.height(),
          width = !isNaN(parseInt($this.attr('width'), 10))
            ? parseInt($this.attr('width'), 10)
            : $this.width(),
          aspectRatio = height / width;
        if (!$this.attr('name')) {
          var videoName = 'fitvid' + $.fn.fitVids._count;
          $this.attr('name', videoName);
          $.fn.fitVids._count++;
        }
        $this
          .wrap('<div class="fluid-width-video-wrapper"></div>')
          .parent('.fluid-width-video-wrapper')
          .css('padding-top', aspectRatio * 100 + '%');
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };

  // Internal counter for unique video names.
  $.fn.fitVids._count = 0;

  // Works with either jQuery or Zepto
})(window.jQuery || window.Zepto);

// APP JS
window.addEventListener('load', function () {
  // Navigation toggle
  // let main_navigation = document.querySelector('#main-menu');
  // document.querySelector('#main-menu-toggle').addEventListener('click', function (e) {
  //   e.preventDefault();
  //   main_navigation.classList.toggle('hidden');
  // });
});

// JQuery
jQuery(function ($) {
  $('#scrollnav').scrollspy({ offset: -75 });

  $('#main-menu-toggle').on('click', function () {
    $('#slide-over').removeClass('-z-10').addClass('z-30');
    $('#menu-overlay').removeClass('opacity-0').addClass('opacity-100');
    $('#main-menu').removeClass('translate-x-full').addClass('translate-x-0');
  });
  $('#main-menu-close, #menu-overlay').on('click', function () {
    $('#menu-overlay').removeClass('opacity-100').addClass('opacity-0');
    $('#main-menu').removeClass('translate-x-0').addClass('translate-x-full');
    setTimeout(function () {
      $('#slide-over').removeClass('z-30').addClass('-z-10');
    }, 500);
  });

  $('#projects-filter button').on('click', function () {
    $project_category = $(this).data('value');
    //console.log($project_category);
    $('#projects-grid').html('');
    $('#projects-loader').removeClass('hidden').addClass('flex');
    $.ajax({
      type: 'POST',
      url: '/wp-admin/admin-ajax.php',
      dataType: 'html',
      data: {
        action: 'project_filter',
        project_category: $project_category,
      },
      success: function (res) {
        //console.log(res);
        $('#projects-loader').removeClass('flex').addClass('hidden');
        $('#projects-grid').html(res);
      },
    });
  });

  $('iframe[src*="youtube"]').parent().fitVids();
});
