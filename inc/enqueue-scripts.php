<?php

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function interdesign_asset($path)
{
  if (wp_get_environment_type() === 'production') {
    return get_stylesheet_directory_uri() . '/' . $path;
  }

  return add_query_arg('time', time(),  get_stylesheet_directory_uri() . '/' . $path);
}

/**
 * Enqueue theme assets.
 */
function interdesign_enqueue_scripts()
{
  $theme = wp_get_theme();

  wp_enqueue_style('swiper', 'https://unpkg.com/swiper@7/swiper-bundle.min.css', array(), $theme->get('Version'));
  wp_enqueue_script('swiper', 'https://unpkg.com/swiper@7/swiper-bundle.min.js', array(), $theme->get('Version'));
  wp_enqueue_script('tw-elements', 'https://cdn.jsdelivr.net/npm/tw-elements/dist/js/index.min.js', array(), $theme->get('Version'));
  wp_enqueue_style('tailpress', interdesign_asset('css/app.css'), array(), THEME_VERSION);
  wp_enqueue_script('tailpress', interdesign_asset('js/app.js'), array('jquery'), THEME_VERSION);
}

add_action('wp_enqueue_scripts', 'interdesign_enqueue_scripts');
