<?php
function product_item($url, $image, $title, $subtitle)
{
  $output = '<a href="' . $url . '" class="relative block rounded-md overflow-hidden bg-stone-200 aspect-square transition duration-500 ease-in-out hover:lg:shadow-xl hover:lg:shadow-stone-900/50 hover:lg:-translate-y-1">';

  $output .= '<img class="absolute inset-0 w-full h-full object-cover" src="' . $image . '">';

  $output .= '<div class="absolute w-full h-full inset-0 bg-gradient-to-t from-stone-900 via-stone-900/25"></div>';
  $output .= '<div class="absolute left-3 bottom-3 right-3 text-white md:left-4 md:bottom-4 md:right-4 lg:left-6 lg:bottom-6 lg:right-12">';

  $output .= '<h4 class="text-base leading-tight font-bold mb-0.5 lg:text-xl lg:mb-1 xl:text-2xl">' . $title . '</h4>';
  $output .= '<p class="text-xs lg:text-sm xl:text-base">' . $subtitle . '</p>';

  $output .= '</div>';

  $output .= '</a>';

  return $output;
}

function interdesign_products_grid()
{
?>
  <div class="grid grid-cols-2 gap-0 lg:grid-cols-3 lg:gap-8">
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'ETFE',
      'Modern way to create shape'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Ceiling & Partition',
      'Drywall innovation'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Data Center',
      'Structural and heavy duty ceiling'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Metal Solution',
      'Modern architectural solution'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Stretched Membrane',
      'Attractive and flexibility of creating shapes'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Acoustical panel',
      'polyester fiber'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Toilet & Shower Cubicle',
      'Modern way of toilet and shower cubicle by iCUBIX'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'Public seating',
      'Creative standard of public seating'
    );
    ?>
    <?php echo
    product_item(
      '#',
      '/wp-content/uploads/2021/12/homeslide-01.jpg',
      'WALGUARD',
      'Surface protection'
    );
    ?>

  </div>
<?php } ?>

<?php

function interdesign_products_grid_2()
{
  $args = array(
    'post_type' => 'products-services',
    'post_parent'       => 0,
    'orderby' => 'menu_order',
    'posts_per_page' => -1,
  );
  $posts_query = new WP_Query($args);
  if ($posts_query->have_posts()) {
    echo '<div class="grid grid-cols-2 gap-1 -mx-3 md:grid-cols-3 md:gap-4 xl:gap-8">';
    while ($posts_query->have_posts()) {
      $posts_query->the_post();
      $id = get_the_ID();
      $url = get_the_permalink();
      $image = get_the_post_thumbnail_url();
      $title = get_the_title();
      $subtitle = get_field('short_description');
      echo product_item(
        $url,
        $image,
        $title,
        $subtitle
      );
    }
    echo '</div>';
  }
}

function interdesign_products_grid_3()
{
  $args = array(
    'post_type' => 'products-services',
    'post_parent'       => 0,
    'orderby' => 'menu_order',
    'posts_per_page' => -1,
  );
  $posts_query = new WP_Query($args);
  if ($posts_query->have_posts()) {

    echo '<div class="grid grid-cols-2 gap-2 lg:grid-cols-3 lg:gap-8">';
    while ($posts_query->have_posts()) {
      $posts_query->the_post();
      echo get_template_part('template-parts/product-grid-item');
    }
    echo '</div>';

    // echo '<div class="grid grid-cols-2 gap-1 -mx-3 md:grid-cols-3 md:gap-4 xl:gap-8">';
    // while ($posts_query->have_posts()) {
    //   $posts_query->the_post();
    //   $id = get_the_ID();
    //   $url = get_the_permalink();
    //   $image = get_the_post_thumbnail_url();
    //   $title = get_the_title();
    //   $subtitle = get_field('short_description');
    //   echo product_item(
    //     $url,
    //     $image,
    //     $title,
    //     $subtitle
    //   );
    // }
    // echo '</div>';
  }
}

function interdesign_projects_filter()
{
  $catSlug = $_POST['project_category'];

  $response = '';
  //$response .= $catSlug;

  $defaults = array(
    'post_type' => 'projects',
    'posts_per_page' => -1,
    'orderby' => 'menu_order',
    'post_status' => 'publish',
    'order' => 'asc',
  );

  if ($catSlug != 'all') {
    $args = array(
      'tax_query' => array(
        array(
          'taxonomy' => 'project_category',
          'field'    => 'slug',
          'terms'    => $catSlug,
        ),
      ),
    );
  }

  $args = wp_parse_args($args, $defaults);
  $ajaxposts = new WP_Query($args);

  // $response .= '<p>';
  // $response .= print_r($ajaxposts);
  // $response .= '</p>';

  if ($ajaxposts->have_posts()) {
    $response .= '<div class="grid grid-cols-2 gap-0 lg:grid-cols-3 lg:gap-8">';
    while ($ajaxposts->have_posts()) : $ajaxposts->the_post();

      $id = get_the_ID();
      $url = get_the_permalink();
      $image = get_the_post_thumbnail_url();
      $title = get_the_title();
      $terms = get_the_terms($id, 'project_category');
      $subtitle = $terms[0]->name;
      $parent_id = $terms[0]->parent;
      if ($parent_id != 0) {
        $parent = get_term($parent_id)->name;
        $subtitle = $parent . ' - ' . $terms[0]->name;
      }

      $response .= '<a href="' . $url . '" class="block rounded-md overflow-hidden transition duration-500 ease-in-out hover:shadow-xl hover:shadow-stone-900/10 hover:-translate-y-0.5">
  <div class="relative bg-stone-200 aspect-square">
    <img class="absolute inset-0 w-full h-full object-cover" src="' . $image . '">
  </div>
  <div class="rounded-b-md px-4 py-4 border-x border-b border-solid border-stone-200 bg-white">
    <h4 class="text-base font-bold mb-1 truncate">' . $title . '</h4>
    <p class="text-[11px] text-stone-600 uppercase truncate">' . $subtitle . '</p>
  </div>
</a>';

    endwhile;
    $response .= '</div>';
  } else {
    $response .= '<div class="text-center text-3xl">Sorry, there\'s no project in this category.</div>';
  }

  echo $response;
  exit;
}
add_action('wp_ajax_project_filter', 'interdesign_projects_filter');
add_action('wp_ajax_nopriv_project_filter', 'interdesign_projects_filter');

function interdesign_pagination()
{

  //if (is_singular())
  //return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if ($wp_query->max_num_pages <= 1)
    return;

  $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
  $max   = intval($wp_query->max_num_pages);

  /** Add current page to the array */
  if ($paged >= 1)
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ($paged >= 3) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if (($paged + 2) <= $max) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="navigation"><ul>' . "\n";

  /** Previous Post Link */
  if (get_previous_posts_link())
    printf('<li>%s</li>' . "\n", get_previous_posts_link());

  /** Link to first page, plus ellipses if necessary */
  if (!in_array(1, $links)) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

    if (!in_array(2, $links))
      echo '<li>…</li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort($links);
  foreach ((array) $links as $link) {
    $class = $paged == $link ? ' class="active"' : '';
    printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
  }

  /** Link to last page, plus ellipses if necessary */
  if (!in_array($max, $links)) {
    if (!in_array($max - 1, $links))
      echo '<li>…</li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
  }

  /** Next Post Link */
  if (get_next_posts_link())
    printf('<li>%s</li>' . "\n", get_next_posts_link());

  echo '</ul></div>' . "\n";
}

//Infinite Scroll
// https://dev.to/erhankilic/adding-infinite-scroll-without-plugin-to-wordpress-theme-595a
function interdesign_infinite_projects()
{
  $paged = $_POST['page_no'];
  $args = array(
    'post_type' => 'projects',
    'posts_per_page' => 6,
    'paged' => $paged,
  );
  # Load the posts
  $posts_query = new WP_Query($args);

  if ($posts_query->have_posts()) {
    while ($posts_query->have_posts()) {
      $posts_query->the_post();
      echo get_template_part('template-parts/project-grid-item');
    }
  }

  exit;
}
add_action('wp_ajax_infinite_scroll', 'interdesign_infinite_projects'); // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'interdesign_infinite_projects'); // if user not logged in