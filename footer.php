</main>

<?php do_action('interdesign_content_end'); ?>

</div>

<?php do_action('interdesign_content_after'); ?>

<footer class="site-footer">
  <?php do_action('interdesign_footer'); ?>

  <?php
  $cta_title = get_field('cta_title', 'option');
  $cta_text = get_field('cta_text', 'option');
  $cta_button_text = get_field('cta_button_text', 'option');
  $cta_button_link = get_field('cta_button_link', 'option');
  $cta_disable_on_page = get_field('cta_disable_on_page', 'option');

  //echo $cta_button_link;
  //echo '<br>';
  //print_r($cta_disable_on_page);

  $page_id = get_the_ID();

  $footer_class = 'pt-12';
  if (!in_array($page_id, $cta_disable_on_page)) {
    $footer_class = '-mt-20 pt-28';
  }

  ?>

  <?php if (!in_array($page_id, $cta_disable_on_page)) { ?>
    <?php if ($cta_title) { ?>
      <div class="container mx-auto relative z-20">
        <div class="bg-white shadow-2xl rounded-xl px-8 py-8 md:px-8 md:py-8 lg:px-14 lg:py-10">
          <div class="flex flex-col flex-wrap gap-6 md:flex-nowrap md:flex-row md:justify-between md:items-center md:gap-8 lg:gap-10">
            <div>
              <?php if ($cta_title) { ?>
                <h3 class="text-[40px] leading-tight font-extrabold mb-2 tracking-tight md:text-[44px] lg:text-5xl"><?php echo $cta_title ?></h3>
              <?php } ?>
              <?php if ($cta_text) { ?>
                <p class="text-lg text-stone-700 lg:text-xl"><?php echo $cta_text ?></p>
              <?php } ?>
            </div>

            <div>
              <?php if ($cta_button_link) { ?>
                <a href="<?php echo $cta_button_link ?>" class="inline-block w-auto px-8 py-4 bg-blue-500 uppercase text-center whitespace-nowrap rounded text-white text-base font-semibold transition duration-300 hover:bg-blue-400 md:w-full lg:px-10"><?php echo $cta_button_text ?></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  <?php } ?>


  <div class="bg-stone-900 pb-12 relative z-10 <?php echo $footer_class ?>">

    <div class="container mx-auto text-stone-300 py-12">
      <div class="grid grid-cols-1 gap-y-8 md:grid-cols-2 md:gap-y-10 md:gap-x-10 xl:grid-cols-4">
        <div>
          <div class="mb-6">
            <img class="max-h-[42px] w-auto mb-4" src="<?php echo get_stylesheet_directory_uri() . '/images/logo-interdesign.svg'; ?>" />
            <?php
            $footer_text = get_field('footer_text', 'option');
            if ($footer_text) {
              echo '<p>' . $footer_text . '</p>';
            }
            ?>
          </div>
          <div>
            <?php
            $instagram_url = get_field('instagram_url', 'option');
            $facebook_url = get_field('facebook_url', 'option');
            $linked_in_url = get_field('linked_in_url', 'option');
            ?>
            <ul class="flex gap-3">
              <?php
              if ($instagram_url) {
                echo '<li><a href="' . $instagram_url . '">'
                  . interdesign_icon(array('group' => 'social', 'icon' => 'instagram', 'title' => 'Instagram', 'size' => '28', 'class' => 'transition duration-300 fill-stone-300 hover:fill-stone-50')) . '</a></li>';
              }
              if ($facebook_url) {
                echo '<li><a href="' . $facebook_url . '">'
                  . interdesign_icon(array('group' => 'social', 'icon' => 'facebook', 'title' => 'Facebook', 'size' => '28', 'class' => 'transition duration-300 fill-stone-300 hover:fill-stone-50')) . '</a></li>';
              }
              if ($linked_in_url) {
                echo '<li><a href="' . $linked_in_url . '">'
                  . interdesign_icon(array('group' => 'social', 'icon' => 'linkedin', 'title' => 'Linked In', 'size' => '28', 'class' => 'transition duration-300 fill-stone-300 hover:fill-stone-50')) . '</a></li>';
              }
              ?>
            </ul>
          </div>
        </div>
        <div class="xl:flex xl:flex-col xl:items-center">
          <div>
            <h4 class="mb-5 text-stone-500 text-lg font-bold">BROWSE</h4>
            <?php
            wp_nav_menu(
              array(
                'container_id'    => 'footer-menu',
                'container_class' => '',
                'menu_class'      => 'flex flex-col gap-y-2 text-sm',
                'theme_location'  => 'primary',
                'li_class'        => '',
                'link_before' => '<span>',
                'link_after' => '</span>',
                'fallback_cb'     => false,
              )
            );
            ?>
          </div>
        </div>
        <div>
          <h4 class="mb-5 text-stone-500 text-lg font-bold">INTERDESIGN GROUP</h4>
          <ul class="flex flex-col gap-y-2 text-sm">
            <?php
            $interdesign_group = get_field('interdesign_group', 'option');
            if ($interdesign_group) {
              $interdesign_list = explode(PHP_EOL, $interdesign_group);
              foreach ($interdesign_list as $list) {
                echo '<li>' . $list . '</li>';
              }
            }
            ?>
          </ul>
        </div>
        <div>
          <h4 class="mb-5 text-stone-500 text-lg font-bold">CONTACT US</h4>
          <ul class="flex flex-col gap-y-2 text-sm">
            <?php
            $email = get_field('email', 'option');
            $address = get_field('address', 'option');
            $google_map_link = get_field('google_map_link', 'option');
            $phone = get_field('phone', 'option');
            $fax = get_field('fax', 'option');
            $whatsapp_01 = get_field('whatsapp_01', 'option');
            $whatsapp_02 = get_field('whatsapp_02', 'option');
            ?>

            <?php if ($whatsapp_01 || $whatsapp_02) { ?>
              <li class="flex">
                <div class="flex-none mr-3 pt-0.5">
                  <?php echo interdesign_icon(array(
                    'icon'  => 'whatsapp',
                    'group'  => 'menu',
                    'size'  => 20,
                    'class'  => 'fill-stone-300',
                  )); ?>
                </div>
                <?php
                $wa_message = 'Halo%2C%20Saya%20mau%20minta%20info%20tentang%20produk%20dan%20layanan%20Interdesign.'
                ?>
                <div>
                  <?php if ($whatsapp_01) { ?>
                    <?php
                    $whatsapp_01_replace = preg_replace('/^0/', '62', $whatsapp_01);
                    $whatsapp_01_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $whatsapp_01_replace);
                    ?>
                    <a href="https://wa.me/<?php echo $whatsapp_01_formatted ?>?text=<?php echo $wa_message ?>" target="_blank" class="hover:underline"><?php echo $whatsapp_01 ?></a>
                    <br />
                  <?php } ?>
                  <?php if ($whatsapp_02) { ?>
                    <?php
                    $whatsapp_02_replace = preg_replace('/^0/', '62', $whatsapp_02);
                    $whatsapp_02_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $whatsapp_02_replace);
                    ?>
                    <a href="https://wa.me/<?php echo $whatsapp_02_formatted ?>?text=<?php echo $wa_message ?>" target="_blank" class="hover:underline"><?php echo $whatsapp_02 ?></a>
                  <?php } ?>
                </div>
              </li>
            <?php } ?>
            <?php if ($address) { ?>
              <li class="flex">
                <div class="flex-none mr-3 pt-0.5">
                  <?php echo interdesign_icon(array(
                    'icon'  => 'location',
                    'group'  => 'menu',
                    'size'  => 20,
                    'class'  => 'fill-stone-300',
                  )); ?>
                </div>
                <div>
                  <?php echo $address ?>
                  <br />
                  <?php if ($google_map_link) { ?>
                    <a href="<?php echo $google_map_link ?>" target="_blank" class="underline hover:underline hover:text-white">Google Map</a>
                  <?php } ?>
                </div>
              </li>
            <?php } ?>
            <?php if ($email) { ?>
              <li class="flex">
                <div class="flex-none mr-3 pt-0.5">
                  <?php echo interdesign_icon(array(
                    'icon'  => 'email',
                    'group'  => 'menu',
                    'size'  => 20,
                    'class'  => 'fill-stone-300',
                  )); ?>
                </div>
                <div><a href="mailto:<?php echo $email ?>" target="_blank" class="hover:underline"><?php echo $email ?></a></div>
              </li>
            <?php } ?>
            <?php if ($phone) { ?>
              <li class="flex">
                <div class="flex-none mr-3 pt-0.5">
                  <?php echo interdesign_icon(array(
                    'icon'  => 'contact',
                    'group'  => 'menu',
                    'size'  => 20,
                    'class'  => 'fill-stone-300',
                  )); ?>
                </div>
                <?php
                $phone_replace = str_replace('(0', '62', $phone);
                $phone_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $phone_replace);
                ?>
                <div><a href="tel:<?php echo $phone_formatted ?>" target="_blank" class="hover:underline"><?php echo $phone ?></a></div>
              </li>
            <?php } ?>
            <?php if ($fax) { ?>
              <li class="flex">
                <div class="flex-none mr-3 pt-0.5">
                  <?php echo interdesign_icon(array(
                    'icon'  => 'fax',
                    'group'  => 'menu',
                    'size'  => 20,
                    'class'  => 'fill-stone-300',
                  )); ?>
                </div>
                <?php
                $fax_replace = str_replace('(0', '62', $fax);
                $fax_formatted = preg_replace('/[^A-Za-z0-9\-]/', '', $fax_replace);
                ?>
                <div><a href="tel:<?php echo $fax_formatted ?>" target="_blank" class="hover:underline"><?php echo $fax ?></a></div>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>

    </div>

    <div class="container mx-auto text-center text-stone-300 text-sm">
      &copy; <?php echo date_i18n('Y'); ?> - Interdesign Group. All rights reserved.
    </div>
  </div>


</footer>

</div>

<?php wp_footer(); ?>

</body>

</html>