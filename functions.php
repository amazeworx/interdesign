<?php

define('THEME_VERSION', filemtime(get_stylesheet_directory() . '/css/app.css'));

/**
 * Load Required functions
 */
require get_template_directory() . '/inc/theme-setup.php';
require get_template_directory() . '/inc/enqueue-scripts.php';
require get_template_directory() . '/inc/helper-functions.php';
require get_template_directory() . '/inc/interdesign-functions.php';
require get_template_directory() . '/inc/navigation.php';
require get_template_directory() . '/inc/acf.php';
