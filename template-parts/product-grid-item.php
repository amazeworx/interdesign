<?php

$id = get_the_ID();
$url = get_the_permalink();
$image = get_the_post_thumbnail_url();
$title = get_the_title();
$terms = get_the_terms($id, 'project_category');
//$subtitle = $terms[0]->name;
$subtitle = get_field('short_description');
// $parent_id = $terms[0]->parent;
// if ($parent_id != 0) {
//   $parent = get_term($parent_id)->name;
//   $subtitle = $parent . ' - ' . $terms[0]->name;
// }
?>

<a href="<?php echo $url ?>" class="block rounded-md overflow-hidden border border-solid border-stone-20 transition duration-500 ease-in-out hover:shadow-xl hover:shadow-stone-900/10 hover:-translate-y-0.5">
  <div class="relative bg-stone-200 aspect-square">
    <img class="absolute inset-0 w-full h-full object-cover" src="<?php echo $image ?>">
  </div>
  <div class="rounded-b-md px-4 py-4 bg-white">
    <h4 class="text-base font-bold mb-1 uppercase"><?php echo $title ?></h4>
    <p class="text-[12px] text-stone-600"><?php echo $subtitle ?></p>
  </div>
</a>