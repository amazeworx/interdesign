<?php

$id = get_the_ID();
$url = get_the_permalink();
$image = get_the_post_thumbnail_url();
$title = get_the_title();
$subtitle = get_the_term_list($id, 'project_category', '', ', ');
$subtitle = strip_tags($subtitle);
?>

<a href="<?php echo $url ?>" class="block rounded-md overflow-hidden transition duration-500 ease-in-out hover:shadow-xl hover:shadow-stone-900/10 hover:-translate-y-0.5">
  <div class="relative bg-stone-200 aspect-square">
    <img class="absolute inset-0 w-full h-full object-cover" src="<?php echo $image ?>">
  </div>
  <div class="rounded-b-md px-4 py-4 border-x border-b border-solid border-stone-200 bg-white">
    <h4 class="text-base font-bold mb-1 truncate"><?php echo $title ?></h4>
    <p class="text-[11px] text-stone-600 uppercase truncate"><?php echo $subtitle ?></p>
  </div>
</a>